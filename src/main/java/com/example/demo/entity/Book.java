package com.example.demo.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
@Where(clause = "end_date is null")
@Table(name = "book", catalog = "library")
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "kitap_id", nullable = false)

    private Integer kitapId;
    private String adi;
    private String basimYili;
    private String baskiNo;
    private String ceviren;
    private String yazarAdi;
    private LocalDate endDate;
}
