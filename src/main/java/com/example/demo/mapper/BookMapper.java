package com.example.demo.mapper;

import com.example.demo.entity.Book;
import com.example.demo.request.BookAddRequest;
import com.example.demo.response.BookResponse;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {
    List<BookResponse> toBookResponseList(List<Book> books);
    BookResponse toBookResponse(Book book);
    Book toBook(BookAddRequest bookAddRequest);
}
