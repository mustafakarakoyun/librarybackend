package com.example.demo.service;


import com.example.demo.request.BookAddRequest;
import com.example.demo.response.BookResponse;

import java.util.List;

public interface BookService {

    List<BookResponse> listAll();
    BookResponse save(BookAddRequest bookAddRequest);
    void delete(Integer kitapId);
}
