package com.example.demo.service.impl;


import com.example.demo.entity.Book;
import com.example.demo.mapper.BookMapper;
import com.example.demo.repository.BookRepository;
import com.example.demo.request.BookAddRequest;
import com.example.demo.response.BookResponse;
import com.example.demo.service.BookService;
import org.springframework.beans.BeanUtils;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;



import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
@Transactional
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    @Override
    public List<BookResponse> listAll() {
        List<Book> book = bookRepository.findAll();
        List<BookResponse> bookResponse = bookMapper.toBookResponseList(book);

        return bookResponse;
    }
    @Override
    public BookResponse save(BookAddRequest bookAddRequest) {
        Book book = bookMapper.toBook(bookAddRequest);
        Book saved = bookRepository.save(book);
        return bookMapper.toBookResponse(saved);
    }
    @Override
    public void delete(Integer kitapId) {
        Optional<Book> book = bookRepository.findById(kitapId);

        if (book.isPresent()) {
            book.get().setEndDate(LocalDate.now());
            bookRepository.save(book.get());
        }

    }
}
