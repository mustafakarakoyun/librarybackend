package com.example.demo.response;

import lombok.Data;
@Data
public class BookResponse {

        private Integer kitapId;
        private String adi;
        private String basimYili;
        private String baskiNo;
        private String ceviren;
        private String yazarAdi;

}
