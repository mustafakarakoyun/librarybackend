package com.example.demo.exception;

public class LibraryException extends Exception {

    public LibraryException(String message) {
        super(message);
    }
}
