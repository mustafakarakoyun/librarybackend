package com.example.demo.exception;

public class ObjectPersistException extends RuntimeException {

    public ObjectPersistException(String message) {
        super(message);
    }
}
