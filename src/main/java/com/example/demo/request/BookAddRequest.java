package com.example.demo.request;

import lombok.Data;

@Data
public class BookAddRequest {
    private String adi;
    private String basimYili;
    private String baskiNo;
    private String ceviren;
    private String yazarAdi;
}
