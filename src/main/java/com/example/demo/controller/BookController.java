package com.example.demo.controller;

import com.example.demo.request.BookAddRequest;
import com.example.demo.response.BookResponse;
import com.example.demo.response.HttpResponseMessage;
import com.example.demo.service.BookService;
import com.example.demo.constants.Constants;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/book")
public class BookController {
    private final MessageSource messageSource;
    private final BookService bookService;

    @GetMapping(value = {""})
    public ResponseEntity<?> getAll(){
        List<BookResponse> bookResponses = bookService.listAll();

        HttpResponseMessage message = new HttpResponseMessage.HttpResponseMessageBuilder()
                .success(true)
                .items(bookResponses)
                .build();
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PostMapping(value = {""})
    public ResponseEntity<?> save(@RequestBody BookAddRequest bookAddRequest){

        BookResponse bookResponse = bookService.save(bookAddRequest);

        HttpResponseMessage message = new HttpResponseMessage.HttpResponseMessageBuilder()
                .success(true)
                .item(bookResponse)
                .build();
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") int kitapId) {
        bookService.delete(kitapId);
        HttpResponseMessage message = new HttpResponseMessage.HttpResponseMessageBuilder()
                .success(true)
                .item(Constants.DELETED_SUCCESS_STATUS)
                .build();
        return new ResponseEntity<>(message, HttpStatus.OK);

    }
}
